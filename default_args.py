import argparse


class DefaultArgs:
    def __init__(self) -> None:
        parser = argparse.ArgumentParser()

        parser.add_argument(
            "--config-file", type=str, help="dir to .cfg file", required=True,
        )

        parser.add_argument(
            "--config-section",
            type=str,
            help="section to use in config.cfg configurations file",
            required=True,
        )

        parser.add_argument(
            "--device", default="cuda:0", type=str, help="cpu | cuda:0 | cuda:1 ..."
        )
        parser.add_argument("--patch-size", default=512, nargs="+", type=int)
        parser.add_argument(
            "--workers", default=0, type=int, help="Dataset worker threads"
        )

        self._parser = parser

    def Parse(self):
        args = self._parser.parse_args()

        self.configFile: str = args.config_file
        self.configSection: str = args.config_section

        self.patchSize: int = args.patch_size
        self.device: str = args.device
        self.datasetWorkers: int = args.workers

        return args
