from __future__ import division
import os
import logging
import sys

import torch
from torchvision import transforms
import torch.nn
import torch.utils.data
from torch import optim
import functools
import imageio
import wandb
import numpy as np

from wandb_wrapper import WandbWrapper
from model_wrapper import ModelWrapper
from config_parser import ConfigParser, RunType
from image_dataset.dataset_loaders.CEL import CELDataloaderFactory, cel_filters
import metric_handlers
from default_args import DefaultArgs
from networks import AdaUNet4X3
import common

IMAGE_BPS: int = 16


class TestArgs(DefaultArgs):
    def __init__(self) -> None:
        super().__init__()

        parser = self._parser

        parser.add_argument("--output-dir", default=None, required=False, type=str)
        parser.add_argument("--save-image-rate", default=10, required=False, type=int)

    def Parse(self):
        args = super().Parse()

        self.outputDir = args.output_dir
        self.imsaveRate = args.save_image_rate


def AddTestCallbacks(
    wrapper: ModelWrapper,
    PSNR: metric_handlers.PSNR,
    SSIM: metric_handlers.SSIM,
    imageNumberMetric: metric_handlers.Metric[int],
):
    def SaveMetrics(
        inputImage: torch.Tensor,
        gTruthImage: torch.Tensor,
        unetOutput: torch.Tensor,
        loss: float,
    ):

        imageNumber = imageNumberMetric.data.__len__()
        imageNumberMetric.Call(imageNumber)

        gtruthProcessed = gTruthImage[0].permute(1, 2, 0).cpu().data.numpy()
        unetOutputProcessed = unetOutput[0].permute(1, 2, 0).cpu().data.numpy()

        unetOutputProcessed = np.minimum(np.maximum(unetOutputProcessed, 0), 1)

        PSNR.Call(gtruthProcessed, unetOutputProcessed)
        SSIM.Call(gtruthProcessed, unetOutputProcessed)

    wrapper.OnTestIter += SaveMetrics


def GetSaveImagesCallback(
    wrapper: ModelWrapper,
    directory: str,
    rate: int,
    prefix: str,
    wandbWrap: WandbWrapper = None,
):

    imageIndex = [0]

    def Callback(inputImage, gTruthImage, unetOutput, loss):
        if (imageIndex[0] % rate) == 0:
            imname = prefix + "_" + imageIndex[0].__str__()
            imdir = directory + "/" + imname + ".jpg"

            convertedImage = unetOutput[0].permute(1, 2, 0).cpu().data.numpy()

            convertedImage = np.minimum(np.maximum(convertedImage, 0), 1)

            convertedImage *= 255
            convertedImage = convertedImage.astype(np.uint8)
            imageio.imwrite(imdir, convertedImage, "jpg")

            if wandbWrap is not None:
                # wandbWrap.Log(
                #     {"Out Images": [wandb.Image(convertedImage, caption=imname)]}
                # )
                wandb.save(imdir)

        imageIndex[0] += 1

    return Callback


def Run(args: TestArgs):

    TRAIN_EXPOSURE: float = 0.1
    TUNE_EXPOSURE: float = 1
    EVAL_IN_EXPOSURE: float = 0.5

    TRAIN_TRUTH_EXPOSURE: float = 10
    EVAL_TRUTH_EXPOSURE: float = 10

    FACTOR = 1

    configs = ConfigParser.Parse(args.configFile, args.configSection)

    exposureNormTransform = common.NormByExposureTime(IMAGE_BPS)

    trainTransform = transforms.Compose(
        [
            common.GetTrainTransforms(
                IMAGE_BPS, args.patchSize, normalize=False, device=args.device
            ),
            exposureNormTransform,
        ]
    )

    testTransforms = transforms.Compose(
        [
            common.GetEvalTransforms(
                IMAGE_BPS, args.patchSize, normalize=False, device=args.device
            ),
            exposureNormTransform,
        ]
    )

    trainFilter = functools.partial(cel_filters.FilterExact, TRAIN_EXPOSURE)
    trainTruthFilter = functools.partial(cel_filters.FilterExact, TRAIN_TRUTH_EXPOSURE)

    tuneFilter = functools.partial(cel_filters.FilterExact, TUNE_EXPOSURE)
    tuneTruthFilter = functools.partial(cel_filters.FilterExact, TRAIN_TRUTH_EXPOSURE)

    evalFilter = functools.partial(cel_filters.FilterExact, EVAL_IN_EXPOSURE)
    evalTruthFilter = functools.partial(cel_filters.FilterExact, EVAL_TRUTH_EXPOSURE)

    # ! no batch since test yet accounts for batches
    # no cache since usually each image is iterated once
    dataloaderFactory = CELDataloaderFactory(
        configs.input,
        trainTransforms=trainTransform,
        testTransforms=testTransforms,
        trainFilter=trainFilter,
        trainTruthFilter=trainTruthFilter,
        tuneFilter=tuneFilter,
        tuneTruthFilter=tuneTruthFilter,
        evalAndTestFilter=evalFilter,
        evalAndTestTruthFilter=evalTruthFilter,
        batch=1,
        cacheLimit=0,
    )

    testDataloader = dataloaderFactory.GetTest()

    network = AdaUNet4X3(adaptive=True)
    # ! setting optimiser in test mode is redundant, build a dummy optim
    optimiser = optim.Adam(network.parameters(), lr=1e-4)
    wrapper = ModelWrapper(network, optimiser, torch.nn.L1Loss(), args.device)

    metricPSNR = metric_handlers.PSNR(name="PSNR")
    metricSSIM = metric_handlers.SSIM(multichannel=True, name="SSIM")
    tuneFactorMetric = metric_handlers.Metric[float](name="Tune factor")
    imageNumberMetric = metric_handlers.Metric[int](name="Image number")

    wandbWrap: WandbWrapper
    modelDir: str
    csvFileDir: str
    imoutDir: str

    if configs.runType == RunType.wandb:
        wandbWrap = WandbWrapper(
            configs.project, configs.id, entity=configs.entity, downloadModel=False
        )
        wandbWrap.RestoreModel()
        modelDir = wandbWrap.modelDir
        csvFileDir = wandbWrap.runDir + "/data.csv"
        imoutDir = wandbWrap.runDir

    elif configs.runType == RunType.local:
        modelDir = configs.localFile
        csvFileDir = args.outputDir + "data.csv"
        imoutDir = args.outputDir
        wandbWrap = None

    else:
        raise Exception("Error getting run type")

    metricsToCsv = metric_handlers.MetricsToCsv(
        csvFileDir, [imageNumberMetric, tuneFactorMetric, metricPSNR, metricSSIM]
    )

    AddTestCallbacks(wrapper, metricPSNR, metricSSIM, imageNumberMetric)

    if not os.path.exists(modelDir):
        raise IOError("File " + modelDir + " not found")
    else:
        wrapper.LoadWeights(modelDir, strictWeightLoad=True, loadOptimiser=False)

    wrapper.OnTestIter += lambda *args: tuneFactorMetric.Call(FACTOR)

    callback = GetSaveImagesCallback(
        wrapper, imoutDir, args.imsaveRate, "factor " + FACTOR.__str__(), wandbWrap,
    )

    wrapper.OnTestIter += callback
    network.InterpolateAndLoadWeights(FACTOR)
    wrapper.Test(testDataloader)
    wrapper.OnTestIter -= callback

    metricsToCsv.Write()

    # save CSV file
    if configs.runType == RunType.wandb:
        wandbWrap.Save(csvFileDir)


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    args = TestArgs()
    args.Parse()

    Run(args)
