import torch
import torch.utils.data

from .cel import (
    CELPair,
    DatasetFilterCallbackType,
    nopFilter,
    CELDatasetLoader,
)
from image_dataset.image_dataset import ImageDataset
from image_dataset.dataset_loaders import ITuneDataloaderFactory

from typing import Callable, List

CARS_EVAL_SCENARIO_START = 240
CARS_TEST_SCENARIO_START = 270


class CELDataloaderFactory(ITuneDataloaderFactory):
    def __init__(
        self,
        inputFolder: str,
        trainTransforms: Callable,
        testTransforms: Callable,
        patchSize: int = 512,
        datasetWorkers: int = 0,
        batch: int = 1,
        cacheLimit: int = 0,
        trainFilter: DatasetFilterCallbackType = nopFilter,
        tuneFilter: DatasetFilterCallbackType = nopFilter,
        evalAndTestFilter: DatasetFilterCallbackType = nopFilter,
        trainTruthFilter: DatasetFilterCallbackType = nopFilter,
        tuneTruthFilter: DatasetFilterCallbackType = nopFilter,
        evalAndTestTruthFilter: DatasetFilterCallbackType = nopFilter,
    ):

        self._dir = inputFolder
        self._trainTransform = trainTransforms
        self._testTransform = testTransforms
        self._patchSize = patchSize
        self._datasetWorkers = datasetWorkers
        self._batch = batch
        self._cacheLimit = cacheLimit

        self._datasetLoaderTrain = CELDatasetLoader(
            inputFolder, trainFilter, trainTruthFilter
        )

        self._datasetLoaderEval = CELDatasetLoader(
            inputFolder, evalAndTestFilter, evalAndTestTruthFilter
        )

        self._datasetLoaderTune = CELDatasetLoader(
            inputFolder, tuneFilter, tuneTruthFilter
        )

    def _FilterScenariosMax(self, maxScenario: int, images: List[CELPair]):
        newList: List[CELPair] = []
        for image in images:
            if image.truthList[0].scenario <= maxScenario:
                newList.append(image)

        return newList

    def _FilterScenariosMin(self, maxScenario: int, images: List[CELPair]):
        newList: List[CELPair] = []
        for image in images:
            if image.truthList[0].scenario > maxScenario:
                newList.append(image)

        return newList

    def GetTrain(self):
        trainSet = self._datasetLoaderTrain.GetSet()

        trainSet = self._FilterScenariosMax(CARS_EVAL_SCENARIO_START, trainSet)
        trainDataset = ImageDataset(
            trainSet, self._trainTransform, cacheLimit=self._cacheLimit
        )

        trainDatasetLoader = torch.utils.data.DataLoader(
            trainDataset,
            batch_size=self._batch,
            shuffle=True,
            num_workers=self._datasetWorkers,
        )
        return trainDatasetLoader

    def GetEval(self):
        evalSet = self._datasetLoaderEval.GetSet()

        evalSet = self._FilterScenariosMin(CARS_EVAL_SCENARIO_START, evalSet)
        evalSet = self._FilterScenariosMax(CARS_TEST_SCENARIO_START, evalSet)
        testDataset = ImageDataset(
            evalSet, self._testTransform, cacheLimit=self._cacheLimit
        )

        testDatasetLoader = torch.utils.data.DataLoader(
            testDataset, batch_size=1, shuffle=False, num_workers=self._datasetWorkers,
        )
        return testDatasetLoader

    def GetTest(self):
        testSet = self._datasetLoaderEval.GetSet()

        testSet = self._FilterScenariosMin(CARS_TEST_SCENARIO_START, testSet)
        testDataset = ImageDataset(
            testSet, self._testTransform, cacheLimit=self._cacheLimit
        )

        testDatasetLoader = torch.utils.data.DataLoader(
            testDataset, batch_size=1, shuffle=False, num_workers=self._datasetWorkers,
        )
        return testDatasetLoader

    def GetTune(self):
        tuneSet = self._datasetLoaderTune.GetSet()

        tuneSet = self._FilterScenariosMax(CARS_EVAL_SCENARIO_START, tuneSet)
        tuneDataset = ImageDataset(
            tuneSet, self._trainTransform, cacheLimit=self._cacheLimit
        )

        tuneDatasetLoader = torch.utils.data.DataLoader(
            tuneDataset,
            batch_size=self._batch,
            shuffle=True,
            num_workers=self._datasetWorkers,
        )
        return tuneDatasetLoader
