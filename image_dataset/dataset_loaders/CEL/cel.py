import glob
import os
import numpy as np

from image_dataset.dataset_loaders import (
    BaseDatasetLoader,
    BaseDatasetPair,
    BaseImage,
    IExposureImage,
)

from typing import Dict, List, Callable, Union


class CELImage(BaseImage, IExposureImage):
    def __init__(self, imagePath: str) -> None:

        basename = os.path.basename(imagePath)
        formatSplit = basename.rsplit(".", 1)
        format = formatSplit[1]

        BaseImage.__init__(self, imagePath, format)

        dataSplit = formatSplit[0].split("_")

        self.scenario = int(dataSplit[1])
        self.index = dataSplit[2]

        exposure = float(dataSplit[3])
        IExposureImage.__init__(self, exposure)

    @classmethod
    def FromArray(cls, array: List[str]):
        imageArray: List[CELImage] = []
        for image in array:
            imageArray.append(cls(image))

        return imageArray


class CELPair(BaseDatasetPair):
    def __init__(
        self,
        trainingImage: Union[CELImage, List[CELImage]],
        truthImage: Union[CELImage, List[CELImage]],
    ) -> None:

        self.trainList: List[CELImage] = self._LoadListOrImage(trainingImage)
        self.truthList: List[CELImage] = self._LoadListOrImage(truthImage)

    def GetPair(self):
        train = self._GetRandomImage(self.trainList)
        truth = self._GetRandomImage(self.truthList)

        return [train, truth]

    def _LoadListOrImage(self, listOrImage: Union[CELImage, List[CELImage]]):
        if isinstance(listOrImage, list):
            return listOrImage
        else:
            return [listOrImage]

    def _GetRandomImage(self, arr):
        randint = np.random.randint(0, arr.__len__())
        return arr[randint]


DatasetFilterCallbackType = Callable[[List[CELImage]], List[CELImage]]
nopFilter: Callable[[List[CELImage]], List[CELImage]] = lambda images: images


class CELDatasetLoader(BaseDatasetLoader):
    def __init__(
        self,
        path: str,
        trainFilter: DatasetFilterCallbackType = nopFilter,
        truthFilter: DatasetFilterCallbackType = nopFilter,
    ) -> None:
        self._dir = path
        self._trainFormat = "ARW"
        self._truthFormat = "tiff"
        self._trainFilter = trainFilter
        self._truthFilter = truthFilter

    def _GrabImages(self, path: str, imageFormat: str):
        return glob.glob(path + "*." + imageFormat)

    def _GeneratePairs(self, trainList: List[CELImage], truthList: List[CELImage]):
        truthDict: Dict[int, List[CELImage]] = {}
        trainDict: Dict[int, List[CELImage]] = {}
        pairs: List[CELPair] = []

        for truth in truthList:
            if truth.scenario in truthDict.keys():
                truthDict[truth.scenario].append(truth)
            else:
                truthDict[truth.scenario] = [truth]

        for train in trainList:
            if train.scenario not in truthDict:
                continue
            if train.scenario in trainDict.keys():
                trainDict[train.scenario].append(train)
            else:
                trainDict[train.scenario] = [train]

        for trainKey in trainDict:
            trainInd = trainDict[trainKey]
            truthInd = truthDict[trainKey]
            newPair = CELPair(trainInd, truthInd)
            pairs.append(newPair)

        return pairs

    def _IncrementOutdoor(self, images: List[CELImage]):
        for image in images:
            image.scenario += 146

    def GetSet(self):
        indoorPath = self._dir + "indoor/"
        outdoorPath = self._dir + "outdoor/"

        indoorTrain = self._GrabImages(indoorPath + "ARW/", self._trainFormat)
        outdoorTrain = self._GrabImages(outdoorPath + "ARW/", self._trainFormat)
        indoorTruth = self._GrabImages(indoorPath + "TIFF/", self._truthFormat)
        outdoorTruth = self._GrabImages(outdoorPath + "TIFF/", self._truthFormat)

        # warn on bad data load
        if (
            indoorTrain.__len__() == 0
            or outdoorTrain.__len__() == 0
            or indoorTruth.__len__() == 0
            or outdoorTruth.__len__() == 0
        ):
            # TODO, this should be printed by a logger
            print(
                "!WARNING! One or more folders of cars dataset hasn't loaded correctly"
            )

        outdoorTrain = CELImage.FromArray(outdoorTrain)
        outdoorTruth = CELImage.FromArray(outdoorTruth)

        self._IncrementOutdoor(outdoorTrain)
        self._IncrementOutdoor(outdoorTruth)

        indoorTrain = CELImage.FromArray(indoorTrain)
        indoorTruth = CELImage.FromArray(indoorTruth)

        trainImages = indoorTrain + outdoorTrain
        truthImages = indoorTruth + outdoorTruth

        train = self._trainFilter(trainImages)
        truth = self._truthFilter(truthImages)

        pairs = self._GeneratePairs(train, truth)

        return pairs
