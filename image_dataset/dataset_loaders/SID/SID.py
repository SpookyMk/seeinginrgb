import glob
import os
import numpy as np
from enum import Enum

from image_dataset.dataset_loaders import BaseDatasetLoader, BaseDatasetPair, BaseImage

from typing import List, Dict

SID_TRAINING_PREFIX: str = "0"
SID_VALIDATION_PREFIX: str = "2"
SID_TEST_PREFIX: str = "1"


class SIDDatasetType(Enum):
    Train = "0"
    Test = "1"
    Validation = "2"


class SIDImage(BaseImage):
    def __init__(self, imagePath: str) -> None:
        basename = os.path.basename(imagePath)

        formatSplit = basename.rsplit(".", 1)
        format = formatSplit[1]

        super().__init__(imagePath, format)

        dataSplit = formatSplit[0].split("_")

        self.scenario = dataSplit[0]
        self.index = dataSplit[1]

        exposureAsStr = dataSplit[2]
        # remove the "s" at the end of the exposure
        self.exposure = float(exposureAsStr[0:-1])


class SIDPair(BaseDatasetPair):
    def __init__(
        self, truthImage: SIDImage, trainingImages: List[SIDImage],
    ):
        self.truthImage = truthImage
        self.trainingImages = trainingImages

    @classmethod
    def _FromPaths(
        cls, truthImagePath: str, trainingImagesPaths: List[str],
    ):
        truthImage = SIDImage(truthImagePath)

        newImageList = []
        for image in trainingImagesPaths:
            newImageList.append(SIDImage(image))

        trainingImages: List[SIDImage] = newImageList

        return cls(truthImage, trainingImages)

    def GetPair(self):
        randomTraining = self._GerRandomImage()

        return [randomTraining, self.truthImage]

    def _GerRandomImage(self):
        randint = np.random.randint(0, self.trainingImages.__len__())
        return self.trainingImages[randint]


class SIDDatasetLoader(BaseDatasetLoader):
    def __init__(
        self,
        trainPath: str,
        trainFormat: str,
        gtruthPath: str,
        gtruthFormat: str,
        datasetType: SIDDatasetType,
    ) -> None:
        self._trainPath = trainPath
        self._gtruthPath = gtruthPath
        self._trainFormat = trainFormat
        self._gtruthFormat = gtruthFormat
        self._datasetType: SIDDatasetType = datasetType

        if datasetType == SIDDatasetType.Train:
            self._loadAsEval = False
        else:
            self._loadAsEval = True

        self.imageTypes = SIDDatasetType

    def _GrabImages(self, path: str, imageFormat: str, imagePrefix: str = ""):
        return glob.glob(path + imagePrefix + "*." + imageFormat)

    def _GetScenarioFromImagePath(self, path: str):
        name = os.path.basename(path)
        return name[0:5]

    # there are many training images per scenario
    def _GenerateImageSetsTraining(
        self, gtruthImageList: List[str], gtruthToTrainingDict: Dict[str, List[str]]
    ):
        imageSetList: List[SIDPair] = []

        for gtruth in gtruthImageList:

            scenario = self._GetScenarioFromImagePath(gtruth)
            if scenario not in gtruthToTrainingDict:
                continue

            trainingImageDataList: List[str] = []

            for image in gtruthToTrainingDict[scenario]:
                trainingImageDataList.append(image)

            imagePair = SIDPair._FromPaths(gtruth, trainingImageDataList)
            imageSetList.append(imagePair)

        return imageSetList

    def _UnpackTrainSetForEval(self, set: List[SIDPair]):
        returnedPairs: List[SIDPair] = []

        for pair in set:
            for train in pair.trainingImages:
                newPair = SIDPair(pair.truthImage, [train])
                returnedPairs.append(newPair)

        return returnedPairs

    def _CreateGtruthToTrainingDict(
        self, gtruthImageList: List[str], trainingImageList: List[str]
    ):
        newDict: Dict[str, List[str]] = {}

        # build gtruth keys
        for image in gtruthImageList:
            scenario = self._GetScenarioFromImagePath(image)
            newDict[scenario] = []

        # add training images to their respective gtruth
        for image in trainingImageList:
            scenario = self._GetScenarioFromImagePath(image)
            newDict[scenario].append(image)

        return newDict

    def GetSet(self):
        training = self._GrabImages(
            self._trainPath, self._trainFormat, self._datasetType.value
        )
        truth = self._GrabImages(
            self._gtruthPath, self._gtruthFormat, self._datasetType.value
        )

        dicty = self._CreateGtruthToTrainingDict(truth, training)
        imageSet = self._GenerateImageSetsTraining(truth, dicty)

        if self._loadAsEval:
            imageSet = self._UnpackTrainSetForEval(imageSet)

        return imageSet
