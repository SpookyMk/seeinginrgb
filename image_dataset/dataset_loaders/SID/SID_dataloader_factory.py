import torch
import torch.utils.data
from torch.utils.data import DataLoader
import numpy as np

from image_dataset.dataset_loaders import IDataloaderFactory
from image_dataset.image_dataset import ImageDataset
from .SID import SIDDatasetLoader, SIDDatasetType, SIDImage

from typing import Callable, List

NOPTransform: Callable = lambda *args: args


OnImageLoadCallableType = Callable[
    [SIDImage, SIDImage, np.ndarray, np.ndarray], List[np.ndarray]
]


def OnImageLoadNOP(trainData, truthData, train, truth):
    return [train, truth]


class SIDDataloaderFactory(IDataloaderFactory):
    def __init__(
        self,
        trainFolder: str,
        trainFormat: str,
        gtruthFolder: str,
        truthFormat: str,
        trainTransforms: Callable = NOPTransform,
        testTransforms: Callable = NOPTransform,
        patchSize: int = 512,
        bps: int = 8,
        datasetWorkers: int = 0,
        batch: int = 1,
        onImageLoad: OnImageLoadCallableType = OnImageLoadNOP,
    ) -> None:

        self.patchSize = patchSize
        self.bps = bps
        self.datasetWorkers = datasetWorkers
        self.batch = batch

        self.trainTransforms = trainTransforms
        self.testTransforms = testTransforms

        self.SIDDatasetTrain = SIDDatasetLoader(
            trainFolder,
            trainFormat,
            gtruthFolder,
            truthFormat,
            SIDDatasetType.Train,
        )
        self.SIDDatasetEval = SIDDatasetLoader(
            trainFolder,
            trainFormat,
            gtruthFolder,
            truthFormat,
            SIDDatasetType.Validation,
        )
        self.SIDDatasetTest = SIDDatasetLoader(
            trainFolder,
            trainFormat,
            gtruthFolder,
            truthFormat,
            SIDDatasetType.Test,
        )

    def GetTrain(self) -> DataLoader:
        trainSet = self.SIDDatasetTrain.GetSet()
        trainDataset: ImageDataset = ImageDataset(
            trainSet, transforms=self.trainTransforms
        )

        trainDatasetLoader = torch.utils.data.DataLoader(
            trainDataset,
            batch_size=self.batch,
            shuffle=True,
            num_workers=self.datasetWorkers,
        )

        return trainDatasetLoader

    def GetEval(self) -> DataLoader:

        evalSet = self.SIDDatasetEval.GetSet()
        evalDataset = ImageDataset[SIDImage](evalSet, transforms=self.testTransforms)

        evalDatasetLoader = torch.utils.data.DataLoader(
            evalDataset, batch_size=1, shuffle=False, num_workers=self.datasetWorkers
        )

        return evalDatasetLoader

    def GetTest(self) -> DataLoader:
        testSet = self.SIDDatasetTest.GetSet()
        trainDataset = ImageDataset[SIDImage](testSet, transforms=self.testTransforms)

        testDatasetLoader = torch.utils.data.DataLoader(
            trainDataset, batch_size=1, shuffle=False, num_workers=self.datasetWorkers
        )

        return testDatasetLoader
