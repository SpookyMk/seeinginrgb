from __future__ import division
import logging
import sys
import os
import torch
import torch.nn
import torch.utils.data
from torch import optim
import numpy as np
import functools
from torchvision import transforms

from default_args import DefaultArgs
import common
from model_wrapper import ModelWrapper
from config_parser import ConfigParser, RunType
from wandb_wrapper import WandbWrapper
import metric_handlers
from networks import AdaUNet4X3
from image_dataset.dataset_loaders.CEL import CELDataloaderFactory, cel_filters

IMAGE_BPS: int = 16


class TrainArgs(DefaultArgs):
    def __init__(self) -> None:
        super().__init__()
        parser = self._parser

        parser.add_argument("--batch", default=1, type=int, help="Batch size per step")
        parser.add_argument("--cache", default=0, type=int, help="Batch size per step")
        parser.add_argument(
            "--eval-rate", default=50, type=int, help="Evaluation rate by epochs"
        )

    def Parse(self):
        args = super().Parse()

        self.batch: int = args.batch
        self.cache: int = args.cache
        self.evalRate: int = args.eval_rate


def AddWandbTrainCallbacks(
    wrapper: ModelWrapper,
    wandbWrap: WandbWrapper,
    evalDatasetLoader: torch.utils.data.DataLoader,
):

    evalPSNR = metric_handlers.PSNR(dataRange=1)
    evalSSIM = metric_handlers.SSIM(multichannel=True, dataRange=1)
    evalLoss = metric_handlers.Metric[float]()
    trainLoss = metric_handlers.Metric[float]()

    def LogData(avgLoss: float, learningRate: float):
        trainLoss.Call(avgLoss)
        wandbWrap.Log({"Learning Rate": learningRate}, upload=False)

    def OnEpoch(epochIndex: int):
        wandbWrap.Log({"Loss": np.mean(trainLoss.data)}, upload=False)
        wandbWrap.Log({"Epoch": epochIndex}, upload=True)
        trainLoss.Flush()

        # save and upload our weights each epoch
        wandbWrap.SaveModel()

        # this runs an evaluation test
        if epochIndex % args.evalRate == 0:

            # reset metrics per eval
            evalPSNR.Flush()
            evalSSIM.Flush()
            evalLoss.Flush()

            wrapper.Test(evalDatasetLoader)

            wandbWrap.Log(
                {
                    "Eval PSNR Mean": np.mean(evalPSNR.data),
                    "Eval SSIM Mean": np.mean(evalSSIM.data),
                    "Eval Loss Mean": np.mean(evalLoss.data),
                },
                upload=True,
            )

    def OnTest(
        inputImage: torch.Tensor,
        gTruthImage: torch.Tensor,
        unetOutput: torch.Tensor,
        loss: float,
    ):

        gtruthProcessed = gTruthImage[0].permute(1, 2, 0).cpu().data.numpy()
        unetOutputProcessed = unetOutput[0].permute(1, 2, 0).cpu().data.numpy()

        unetOutputProcessed = np.minimum(np.maximum(unetOutputProcessed, 0), 1)

        evalPSNR.Call(gtruthProcessed, unetOutputProcessed)
        evalSSIM.Call(gtruthProcessed, unetOutputProcessed)
        evalLoss.Call(loss)

    wrapper.OnTrainIter += LogData
    wrapper.OnTrainEpoch += OnEpoch

    wrapper.OnTestIter += OnTest


def Run(args: TrainArgs):

    TRAIN_EXPOSURE: float = 0.1
    TUNE_EXPOSURE: float = 0.1
    TRUTH_EXPOSURE: float = 10
    EVAL_EXPOSURE: float = 1

    configs = ConfigParser.Parse(args.configFile, args.configSection)

    exposureNormTransform = common.NormByExposureTime(
        IMAGE_BPS
    )

    trainTransform = transforms.Compose(
        [
            common.GetTrainTransforms(
                IMAGE_BPS, args.patchSize, normalize=False, device=args.device
            ),
            exposureNormTransform,
        ]
    )

    testTransforms = transforms.Compose(
        [
            common.GetEvalTransforms(
                IMAGE_BPS, args.patchSize, normalize=False, device=args.device
            ),
            exposureNormTransform,
        ]
    )

    trainFilter = functools.partial(cel_filters.FilterExact, TRAIN_EXPOSURE)
    trainTruthFilter = functools.partial(cel_filters.FilterExact, TRUTH_EXPOSURE)

    tuneFilter = functools.partial(cel_filters.FilterExact, TUNE_EXPOSURE)
    tuneTruthFilter = functools.partial(cel_filters.FilterExact, TRUTH_EXPOSURE)

    evalFilter = functools.partial(cel_filters.FilterExact, EVAL_EXPOSURE)
    evalTruthFilter = functools.partial(cel_filters.FilterExact, TRUTH_EXPOSURE)

    dataloaderFactory = CELDataloaderFactory(
        configs.input,
        trainTransforms=trainTransform,
        testTransforms=testTransforms,
        trainFilter=trainFilter,
        trainTruthFilter=trainTruthFilter,
        tuneFilter=tuneFilter,
        tuneTruthFilter=tuneTruthFilter,
        evalAndTestFilter=evalFilter,
        evalAndTestTruthFilter=evalTruthFilter,
        batch=args.batch,
        cacheLimit=args.cache,
    )

    wandbWrap: WandbWrapper
    modelDir: str

    if configs.runType == RunType.wandb:
        wandbWrap = WandbWrapper(configs.project, configs.id, configs.entity)
        modelDir = wandbWrap.modelDir

    elif configs.runType == RunType.local:
        modelDir = configs.localFile

    else:
        raise Exception("Error getting run type")

    network = AdaUNet4X3(adaptive=False)

    optimiser = optim.Adam(network.parameters(), lr=1e-4)
    wrapper = ModelWrapper(network, optimiser, torch.nn.L1Loss(), args.device)

    if not os.path.exists(modelDir):
        network._initialize_weights()
        wrapper.Save(modelDir, {"model_tune_state": False})
        if configs.runType == RunType.wandb:
            wandbWrap.SaveModel()

    # TODO, checkpoint is loaded twice, once by wrapper and this,
    # TODO maybe extend wrapper for tunable models?
    checkpoint = torch.load(modelDir, map_location="cpu")
    isModelInTuneState = checkpoint["META"]["model_tune_state"]

    wrapper.OnTrainEpoch += lambda *args: wrapper.Save(modelDir)

    if not isModelInTuneState:

        if configs.runType == RunType.wandb:
            evalDatasetLoader = dataloaderFactory.GetEval()
            AddWandbTrainCallbacks(wrapper, wandbWrap, evalDatasetLoader)
            wandbWrap.WatchModel(network)

        wrapper.LoadWeights(modelDir, strictWeightLoad=True)

        trainDataloader = dataloaderFactory.GetTrain()
        wrapper.Train(trainDataloader, trainToEpoch=1000, learningRate=1e-4)
        wrapper.Train(trainDataloader, trainToEpoch=2000, learningRate=1e-5)
        wrapper.Train(trainDataloader, trainToEpoch=3000, learningRate=1e-6)
        wrapper.Train(trainDataloader, trainToEpoch=4000, learningRate=1e-7)

        # free up memory
        trainDataloader = None

        wrapper.Save(modelDir, {"model_tune_state": True})

    # tuning starts here, rebuild everything
    tuneDataloader = dataloaderFactory.GetTune()

    network = AdaUNet4X3(adaptive=True)
    optimParams = network.TrainingTuneMode()

    optimiser = optim.Adam(optimParams, lr=1e-4)
    wrapper = ModelWrapper(network, optimiser, torch.nn.L1Loss(), args.device)
    wrapper.LoadWeights(modelDir, loadOptimiser=False, strictWeightLoad=False)

    wrapper.OnTrainEpoch += lambda *args: wrapper.Save(modelDir)

    if configs.runType == RunType.wandb:
        evalDatasetLoader = dataloaderFactory.GetEval()
        AddWandbTrainCallbacks(wrapper, wandbWrap, evalDatasetLoader)
        wandbWrap.WatchModel(network)

    wrapper.Train(tuneDataloader, trainToEpoch=4500, learningRate=1e-4)
    wrapper.Train(tuneDataloader, trainToEpoch=5000, learningRate=1e-5)
    wrapper.Train(tuneDataloader, trainToEpoch=5500, learningRate=1e-6)


if __name__ == "__main__":

    logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    args = TrainArgs()
    args.Parse()

    Run(args)
