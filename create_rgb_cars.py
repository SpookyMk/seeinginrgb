# ./venv/bin/activate

import sys
import logging
import cv2
import imageio
import rawpy
import rawpy._rawpy as _rawpy
import os
import glob
import numpy as np
from multiprocessing import Process
import argparse

from typing import List, Callable, Dict


class ConvertRGB:
    def __init__(
        self,
        inputDir: str,
        outputDir: str,
        imageFormatSuffix: str,
        workersTotal: int = 1,
        workerIndex: int = 1,
        outputBPS: int = 8,
    ):
        self.inputDir: str = inputDir
        self.outputDir: str = outputDir

        self._imageFormatSuffix: str = imageFormatSuffix

        self.images: List[str] = []

        self.gtruthExposureDict: Dict[str, float] = {}

        self._workersTotal: int = workersTotal
        self._workerIndex: int = workerIndex

        self._bps = outputBPS

        self._logger = logging.getLogger(__name__ + " " + workerIndex.__str__() + " ")
        self._logger.setLevel(logging.INFO)

        self._Load()

    def _Load(self):
        self._logger.info("Attempting to read training data from " + self.inputDir)

        self.images = self._ImagesInDir(self.inputDir)

        self._logger.info(str(self.images.__len__()) + " training images loaded")

    def _ImagesInDir(self, directory: str) -> List[str]:
        return [
            os.path.basename(pathname)
            for pathname in glob.glob(directory + "*." + self._imageFormatSuffix)
        ]

    def _ConvertFolder(
        self,
        inputDirectory: str,
        imageNameArray: List[str],
        outputDirectory: str,
        imagePostProcess: Callable[
            [_rawpy.RawPy, "ConvertRGB", str, int], _rawpy.RawPy
        ] = lambda image, imageName, exposureDict, bps: image,
    ):

        imageNames = imageNameArray
        path = inputDirectory

        totalImages = imageNames.__len__()

        imagesPerWorker = int(np.floor(totalImages / float(self._workersTotal)))
        startingImage = (self._workerIndex - 1) * imagesPerWorker

        # redundant, but helps understanding, especially with the last thread code
        imagesToProcess = imagesPerWorker

        # we are the last worker, process the rest, can be more since we round down imagesPerWorker
        if self._workerIndex == self._workersTotal:
            imagesToProcess = totalImages - startingImage

        self._logger.info("Converting images in " + path + " to " + outputDirectory)

        currentImage = -1
        for imageName in imageNames:

            currentImage += 1
            if currentImage < startingImage:
                continue

            if currentImage == (startingImage + imagesToProcess):
                return

            self._logger.info(
                str(startingImage)
                + " | "
                + str(currentImage)
                + " | "
                + str(startingImage + imagesToProcess)
            )

            image: _rawpy.RawPy = rawpy.imread(path + imageName)
            convertedImage = imagePostProcess(image, self, imageName, self._bps)
            savename = imageName[:-4]
            imageio.imsave(outputDirectory + savename + ".tiff", convertedImage)

    def ConvertAll(
        self,
        postProcess: Callable[
            [_rawpy.RawPy, "ConvertRGB", str, int], _rawpy.RawPy
        ] = lambda image, imageName, exposureDict, bps: image,
    ):
        self._ConvertFolder(self.inputDir, self.images, self.outputDir, postProcess)

    def TestImage(self):
        self._logger.info("Read " + str(self.images[1].__len__()) + " images")
        impath = self.inputDir + self.images[0]

        image: _rawpy.RawPy = rawpy.imread(impath)

        convertedImage: np.ndarray = image.postprocess()

        cv2.imshow("Winny", convertedImage)
        cv2.waitKey()


def PostProcess(
    image: _rawpy.RawPy, converter: ConvertRGB, imageName: str, outputBPS
) -> _rawpy.RawPy:
    return image.postprocess(
        use_camera_wb=True, half_size=False, no_auto_bright=True, output_bps=outputBPS
    )


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    parser = argparse.ArgumentParser()
    parser.add_argument("--workers", default=4, type=int, help="Number of threads")
    parser.add_argument("--input", type=str, required=True, help="Input SID short folder")
    parser.add_argument(
        "--output", type=str, required=True, help="Output SID short folder"
    )
    parser.add_argument("--bps", type=int, required=True, help="Output bps")

    args = parser.parse_args()

    workersTotal = args.workers

    inputDir: str = args.input
    outputDir: str = args.output

    INPUT_FORMAT: str = "ARW"
    bps: int = args.bps

    print("Running " + str(workersTotal) + " worker threads")
    workerPool: List[Process] = []

    for workerIndex in range(1, workersTotal + 1):

        print("Starting worker " + str(workerIndex))

        converter = ConvertRGB(
            inputDir,
            outputDir,
            INPUT_FORMAT,
            workersTotal=workersTotal,
            workerIndex=workerIndex,
            outputBPS=bps,
        )

        newWorker = Process(target=converter.ConvertAll, args=(PostProcess,))

        workerPool.append(newWorker)
        newWorker.start()

    for worker in workerPool:
        worker.join()
