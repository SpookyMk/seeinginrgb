# Adaunet

# Usage
python3 -m venv venv
pip3 install --upgrade pip
pip3 install -r pip-requirements.txt

# Dev setup
pip3 install -r pip-requirements-dev.txt

This project was built entrely under Vscode with heavy usage of pylance and (a pinch) if mypy.
This project was built to support Weights and Biases (wandb).
It is heavily recommended to use stubgen to generate (at least some) typings for cv2, numpy and wandb.
