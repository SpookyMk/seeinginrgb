import wandb
import os

from typing import Dict, Any

MODEL_NAME = "model.pt"


class WandbWrapperDetached:
    def __init__(self, projectName: str, entity: str, id: str):

        self.run = wandb.init(project=projectName, resume=False, entity=entity, id=id)
        self.runDir = wandb.run.dir

        self._api = wandb.Api()
        self._runApi = self._api.run(self.run.path)

    def Save(self, filename: str):
        wandb.save(filename)

    def Restore(self, filename: str, replace=False):
        wandb.restore(filename, self.run.path, replace=replace)

    def Log(self, logDict: Dict[str, Any], upload: bool = True):
        wandb.log(logDict, commit=upload)


class WandbWrapper(WandbWrapperDetached):

    def __init__(
        self, projectName: str, id: str, entity: str = None, downloadModel=True
    ):

        self.run = wandb.init(project=projectName, resume="allow", entity=entity, id=id)

        self.runDir = wandb.run.dir
        self.modelDir = os.path.join(self.runDir, MODEL_NAME)

        if downloadModel and self.run.resumed:
            self.RestoreModel()

        self._api = wandb.Api()
        self._runApi = self._api.run(self.run.path)

    def RestoreModel(self):
        self.Restore(MODEL_NAME, replace=True)

    def SaveModel(self):
        self.Save(MODEL_NAME)

    def WatchModel(self, model):
        wandb.watch(model)
