# ./venv/bin/activate

import sys
import logging
import cv2
import imageio
import rawpy
import rawpy._rawpy as _rawpy
import os
import glob
import numpy as np
from multiprocessing import Process
import argparse

from typing import List, Callable, Dict


class ConvertRGB:
    def __init__(
        self,
        inputDir: str,
        outputDir: str,
        gtruthInputDir: str,
        gtruthOutputDir: str,
        imageFormatSuffix: str,
        workersTotal: int = 1,
        workerIndex: int = 1,
        outputBPS: int = 8,
    ):
        self.inputDir: str = inputDir
        self.outputDir: str = outputDir
        self.gtruthDir: str = gtruthInputDir
        self.goutDir: str = gtruthOutputDir

        self._imageFormatSuffix: str = imageFormatSuffix

        self.trainingImages: List[str] = []
        self.gtruthImages: List[str] = []

        self.gtruthExposureDict: Dict[str, float] = {}

        self._workersTotal: int = workersTotal
        self._workerIndex: int = workerIndex

        self._bps = outputBPS

        self._logger = logging.getLogger(__name__ + " " + workerIndex.__str__() + " ")
        self._logger.setLevel(logging.INFO)

        self._Load()

    def _Load(self):
        self._logger.info("Attempting to read training data from " + self.inputDir)
        self._logger.info("Attempting to read ground truth data from " + self.gtruthDir)

        self.trainingImages = self._ImagesInDir(self.inputDir)
        self.gtruthImages = self._ImagesInDir(self.gtruthDir)

        self._logger.info(
            str(self.trainingImages.__len__()) + " training images loaded"
        )
        self._logger.info(
            str(self.gtruthImages.__len__()) + " ground truth images loaded"
        )

        self.gtruthExposureDict = self._BuildExposureDict(self.gtruthImages)

    def GetExposure(self, name: str):
        return float(name[9 : (-2 - self._imageFormatSuffix.__len__())])

    def _BuildExposureDict(self, names: List[str]):
        outDict: Dict[str, float] = {}

        for name in names:
            exposure = self.GetExposure(name)
            outDict[name[:5]] = exposure

        return outDict

    def _ImagesInDir(self, directory: str) -> List[str]:
        return [
            os.path.basename(pathname)
            for pathname in glob.glob(directory + "*." + self._imageFormatSuffix)
        ]

    def _ConvertFolder(
        self,
        inputDirectory: str,
        imageNameArray: List[str],
        outputDirectory: str,
        imagePostProcess: Callable[
            [_rawpy.RawPy, "ConvertRGB", str, int], _rawpy.RawPy
        ] = lambda image, imageName, exposureDict, bps: image,
    ):

        imageNames = imageNameArray
        path = inputDirectory

        totalImages = imageNames.__len__()

        imagesPerWorker = int(np.floor(totalImages / float(self._workersTotal)))
        startingImage = (self._workerIndex - 1) * imagesPerWorker

        # redundant, but helps understanding, especially with the last thread code
        imagesToProcess = imagesPerWorker

        # we are the last worker, process the rest, can be more since we round down imagesPerWorker
        if self._workerIndex == self._workersTotal:
            imagesToProcess = totalImages - startingImage

        self._logger.info("Converting images in " + path + " to " + outputDirectory)

        currentImage = -1
        for imageName in imageNames:

            currentImage += 1
            if currentImage < startingImage:
                continue

            if currentImage == (startingImage + imagesToProcess):
                return

            self._logger.info(
                str(startingImage)
                + " | "
                + str(currentImage)
                + " | "
                + str(startingImage + imagesToProcess)
            )

            image: _rawpy.RawPy = rawpy.imread(path + imageName)
            convertedImage = imagePostProcess(image, self, imageName, self._bps)
            savename = imageName[:-4]
            imageio.imsave(outputDirectory + savename + ".tiff", convertedImage)

    def ConvertAll(
        self,
        trainingPostprocess: Callable[
            [_rawpy.RawPy, "ConvertRGB", str, int], _rawpy.RawPy
        ] = lambda image, imageName, exposureDict, bps: image,
        gtruthPostprocess: Callable[
            [_rawpy.RawPy, "ConvertRGB", str, int], _rawpy.RawPy
        ] = lambda image, imageName, exposureDict, bps: image,
    ):
        self._ConvertFolder(
            self.inputDir, self.trainingImages, self.outputDir, trainingPostprocess
        )

        self._ConvertFolder(
            self.gtruthDir, self.gtruthImages, self.goutDir, gtruthPostprocess
        )

    def TestImage(self):
        self._logger.info("Read " + str(self.trainingImages[1].__len__()) + " images")
        impath = self.inputDir + self.trainingImages[0]

        image: _rawpy.RawPy = rawpy.imread(impath)

        convertedImage: np.ndarray = image.postprocess()

        cv2.imshow("Winny", convertedImage)
        cv2.waitKey()


# specifically for SID
def TrainingPostprocess(
    image: _rawpy.RawPy, converter: ConvertRGB, imageName: str, outputBPS: int
) -> _rawpy.RawPy:

    scenarioName = imageName[:5]
    exposureInGtruth = converter.gtruthExposureDict[scenarioName]
    exposureInImage = converter.GetExposure(imageName)
    exposureRatio = exposureInGtruth / exposureInImage

    return image.postprocess(
        use_camera_wb=True,
        half_size=False,
        no_auto_bright=True,
        output_bps=outputBPS,
        bright=exposureRatio,
    )


def GtruthPostprocess(
    image: _rawpy.RawPy, converter: ConvertRGB, imageName: str, outputBPS
) -> _rawpy.RawPy:
    return image.postprocess(
        use_camera_wb=True, half_size=False, no_auto_bright=True, output_bps=outputBPS
    )


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    parser = argparse.ArgumentParser()
    parser.add_argument("--workers", default=4, type=int, help="Number of threads")
    parser.add_argument("--format", default="ARW", type=str, help="Input format")
    parser.add_argument(
        "--in-short", type=str, required=True, help="Input SID short folder"
    )
    parser.add_argument(
        "--in-long", type=str, required=True, help="Input SID long folder"
    )
    parser.add_argument(
        "--out-short", type=str, required=True, help="Output SID short folder"
    )
    parser.add_argument(
        "--out-long", type=str, required=True, help="Output SID long folder"
    )
    parser.add_argument("--bps", type=int, required=True, help="Output bps")

    args = parser.parse_args()

    workersTotal = args.workers

    inputDir: str = args.in_short
    gtruthDir: str = args.in_long

    outputDir: str = args.out_short
    goutDir: str = args.out_long

    inputFormat: str = args.format
    bps: int = args.bps

    print("Running " + str(workersTotal) + " worker threads")
    workerPool: List[Process] = []

    for workerIndex in range(1, workersTotal + 1):

        print("Starting worker " + str(workerIndex))

        converter = ConvertRGB(
            inputDir,
            outputDir,
            gtruthDir,
            goutDir,
            inputFormat,
            workersTotal=workersTotal,
            workerIndex=workerIndex,
            outputBPS=bps,
        )

        newWorker = Process(
            target=converter.ConvertAll, args=(TrainingPostprocess, GtruthPostprocess)
        )

        workerPool.append(newWorker)
        newWorker.start()

    for worker in workerPool:
        worker.join()
