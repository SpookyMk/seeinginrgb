import os
from glob import glob


def RenameFolder(folder: str, prefix: str, numberFrom: int):
    underscorsInFolderName = folder.count("_")
    scenarioUnderscoreSplitIndex = underscorsInFolderName + 1
    images = glob(folder + prefix + "*")
    currentWrittenScenario = numberFrom

    lastScenario = images[0].split("_")[scenarioUnderscoreSplitIndex]

    for imageIter in images:

        imsplit = imageIter.split("_")
        currentScenario = imsplit[scenarioUnderscoreSplitIndex]
        if currentScenario != lastScenario:
            lastScenario = currentScenario
            currentWrittenScenario += 1
        imsplit[scenarioUnderscoreSplitIndex] = currentWrittenScenario.__str__()
        newImName = "_".join(imsplit)
        os.rename(imageIter, newImName)


PREFIX = "indor_"
NUMBER_FROM = 146


RenameFolder("./ARW/", PREFIX, NUMBER_FROM)
RenameFolder("./JPG/", PREFIX, NUMBER_FROM)
RenameFolder("./jpg_fixed/", PREFIX, NUMBER_FROM)
RenameFolder("./tiff/", PREFIX, NUMBER_FROM)