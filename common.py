from torchvision import transforms
import numpy as np

from image_dataset import dataset_transforms
from image_dataset.dataset_loaders import IExposureImage


class NormByExposureTime(
    dataset_transforms._DualImageMetaTransformer
):
    def __init__(self, truthImageBps: int):
        self.truthImageBps: int = truthImageBps

    def _Apply(
        self,
        trainImage: np.ndarray,
        truthImage: np.ndarray,
        trainingData: IExposureImage,
        truthData: IExposureImage,
    ):
        exposureRatio = (float(truthData.exposure) / trainingData.exposure) / (
            16383 - 512
        )
        truthImage = truthImage / float(2 ** self.truthImageBps - 1)
        # to float and subtract black level
        trainImage = trainImage - 512
        trainImage *= exposureRatio
        trainImage = trainImage.clamp(0, 1)

        return [trainImage, truthImage]


def GetTrainTransforms(
    rgbBps: float, patchSize: int, normalize: bool, device: str
):

    transform = transforms.Compose(
        [
            dataset_transforms.BayerUnpack(applyTrain=True, applyTruth=False),
            dataset_transforms.RandomCropRAWandRGB(patchSize),
            dataset_transforms.RandomFlip(),
            dataset_transforms.ToTensor(),
            dataset_transforms.Permute(2, 0, 1),
            dataset_transforms.ToDevice(device),
        ]
    )

    if normalize:

        normTransforms = transforms.Compose(
            [
                dataset_transforms.Normalize(
                    0, 2 ** rgbBps - 1, applyTrain=False, applyTruth=True
                ),
                dataset_transforms.Normalize(
                    512, 16383 - 512, applyTrain=True, applyTruth=False
                ),
            ]
        )

        transform = transforms.Compose([normTransforms, transform])

    return transform


def GetEvalTransforms(
    rgbBps: float, patchSize: int, normalize: bool, device: str
):

    transform = transforms.Compose(
        [
            dataset_transforms.BayerUnpack(applyTrain=True, applyTruth=False),
            dataset_transforms.CenterCropRAWandRGB(patchSize),
            dataset_transforms.ToTensor(),
            dataset_transforms.Permute(2, 0, 1),
            dataset_transforms.ToDevice(device),
        ]
    )

    if normalize:

        normTransforms = transforms.Compose(
            [
                dataset_transforms.Normalize(
                    0, 2 ** rgbBps - 1, applyTrain=False, applyTruth=True
                ),
                dataset_transforms.Normalize(
                    512, 16383 - 512, applyTrain=True, applyTruth=False
                ),
            ]
        )

        transform = transforms.Compose([normTransforms, transform])

    return transform

