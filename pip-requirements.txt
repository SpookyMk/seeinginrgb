# pip reqs
torch
torchvision
numpy
scipy
scikit-image
rawpy
opencv-python
imageio
pytorch_model_summary
pillow
requests
wandb
colour-demosaicing