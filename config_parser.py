import configparser
from enum import Enum, auto

from typing import List


class ConfigEnum(Enum):
    @classmethod
    def Parse(cls, confs: configparser.SectionProxy, field: str):

        value = confs.get(field)
        if value not in cls.__members__.keys():
            allowedKeys: List[str] = []

            for key in cls.__members__.keys():
                allowedKeys.append(key)

            raise Exception(
                value
                + " is not a valid configuration for "
                + field
                + ", please use either of: "
                + allowedKeys.__repr__()
            )
        return cls[value]


class RunType(ConfigEnum):
    local = auto()
    wandb = auto()


class ModelConfigs:
    def __init__(self, confs: configparser.SectionProxy):
        self._confs = confs

        self.runType = RunType.Parse(confs, "run_type")

        self.input = self._Get("input", required=True)
        self.groundTruth = self._Get("ground_truth", required=True)

        wandbConfsRequired = self.runType == RunType.wandb
        wandbConfsMissingPrefix = "run_type is set to: " + RunType.wandb.name + " :"
        self.project = self._Get(
            "project", required=wandbConfsRequired, prefix=wandbConfsMissingPrefix
        )
        self.id = self._Get("id", required=False)
        self.entity = self._Get("entity", required=False)

        localConfsRequired = self.runType == RunType.local
        localConfsMissingPrefix = "run_type is set to: " + RunType.local.name + " :"

        self.localFile = self._Get(
            "local_file", required=localConfsRequired, prefix=localConfsMissingPrefix
        )

        self.localOutputs = self._Get(
            "local_outputs_folder",
            required=False,
            default="./output/",
            prefix=localConfsMissingPrefix,
        )

    def _Get(
        self,
        field: str,
        required: bool = False,
        default: str = "",
        prefix: str = "",
        suffix: str = "",
    ) -> str:

        value = self._confs.get(field)

        if value is None:
            if required is True:
                raise Exception(
                    prefix
                    + "Field "
                    + field
                    + " in configurations file is required"
                    + suffix
                )
            else:
                return default

        return value


class ConfigParser:
    @staticmethod
    def Parse(fileName: str, section: str) -> ModelConfigs:
        config = configparser.ConfigParser(allow_no_value=True, defaults=None)
        config["DEFAULT"] = {}

        read = config.read(fileName)

        if read.__len__() == 0:
            return ModelConfigs(config["DEFAULT"])

        return ModelConfigs(config[section])
